import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store/store'
import VueHighlightJS from 'vue-highlight.js'

import 'vue-highlight.js/lib/allLanguages'

import 'highlight.js/styles/ocean.css'

const moment = require('moment')
require('moment/locale/pt-br')

Vue.use(require('vue-cookies'))
Vue.use(require('vue-sweetalert2'))
Vue.use(require('vue-moment'), {
  moment
})
Vue.use(require('vue-underscore'))
Vue.use(require('vue-clipboard2'))
Vue.use(VueHighlightJS)

import Services from './plugins/Services'

Vue.use(Services)

Vue.config.productionTip = false
Vue.config.silent = true

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
