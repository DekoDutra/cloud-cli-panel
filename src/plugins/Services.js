import services from '@/services'

export default {
  install(Vue) {
    Vue.prototype.$services = {
      user: services.userService,
      teams: services.equipeService
    }
  }
}
