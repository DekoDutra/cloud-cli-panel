import Vue from 'vue'
import Vuetify, { VLayout } from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

import pt from 'vuetify/lib/locale/pt'

Vue.use(Vuetify, {
  iconfont: 'md',
  components: { VLayout },
  theme: {
    primary: '#3a6182'
  },
  lang: {
    locales: { pt },
    current: 'pt'
  }
})
