import Vue from 'vue'
import Router from 'vue-router'
import VueCookies from 'vue-cookies'

Vue.use(Router)
Vue.use(VueCookies)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/Home.vue')
    },
    {
      path: '/usuario',
      name: 'usuario',
      component: () => import('./views/Usuario.vue')
    },
    {
      path: '/equipes',
      name: 'equipes',
      component: () => import('./views/Equipes.vue')
    },
    {
      path: '/equipe/:equipe',
      name: 'equipe',
      component: () => import('./views/childs/ManageTeam.vue')
    },
    {
      path: '/galhos',
      name: 'galhos',
      component: () => import('./views/Galhos.vue')
    },
    {
      path: '/twig/:twig',
      name: 'twig',
      component: () => import('./views/childs/GerenciarTwig.vue')
    },
    {
      path: '/lagos',
      name: 'lagos',
      component: () => import('./views/Lagos.vue')
    },
    {
      path: '/lake/:lake',
      name: 'lake',
      component: () => import('./views/childs/GerenciarLago.vue')
    },
    {
      path: '/suporte',
      name: 'suporte',
      component: () => import('./views/Suporte.vue')
    },
    {
      path: '/teste',
      name: 'teste',
      component: () => import('./views/Teste.vue')
    },
    {
      path: '/leaf/:leaf',
      name: 'leaf',
      component: () => import('./views/childs/GerenciarFolha.vue')
    },
    {
      path: '/arquivos/:lake',
      name: 'arquivos',
      component: () => import('./views/childs/GerenciarArquivos.vue')
    },
    {
      path: '/postgre/:postgre',
      name: 'postgre',
      component: () => import('./views/childs/GerenciarPostgre.vue')
    },
    {
      path: '/mysql/:mysql',
      name: 'mysql',
      component: () => import('./views/childs/GerenciarMysql.vue')
    }
  ]
})
