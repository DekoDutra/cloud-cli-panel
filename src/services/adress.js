import { http } from './config'

export default {
  async novoEndereco(adress) {
    const response = await http.post('enderecos/novo-endereco', adress)
    return response.data
  },

  async buscarEnderecoPorId(id) {
    const response = await http.get(`enderecos/buscar-endereco-por-id/${id}`)
    return response.data
  },

  async atualizarEndereco(adress) {
    const response = await http.put(
      `enderecos/atualizar-endereco/${adress.id}`,
      adress
    )
    return response.data
  },

  async deletarEndereco(id) {
    const response = await http.delete(`enderecos/deletar-endereco/${id}`)
    return response.data
  },

  async listarEnderecosUsuario() {
    const response = await http.get('enderecos/listar-enderecos-usuario')
    return response.data
  },

  async alterarDefaultEndereco(id) {
    const response = await http.put(`enderecos/alterar-default/${id}`)
    return response.data
  }
}
