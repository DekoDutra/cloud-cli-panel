import axios from 'axios'

const API_URL = 'https://cloud-cli.tree-sa-cf.cloudforestdns.com/'
//const API_URL = 'http://localhost:8059'

export const http = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-type': 'application/json',
    Authorization: window.$cookies.get('USERTOKEN')
  }
})

export function parseJwt(token) {
  // The code and explication of this can be found in https://bit.ly/2O85B9z
  let base64Url = token.split('.')[1]
  let base64 = base64Url.replace('-', '+').replace('_', '/')
  return JSON.parse(window.atob(base64))
}
