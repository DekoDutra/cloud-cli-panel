import { http } from './config'

export default {
  async buscarEnviados() {
    const response = await http.get('convites/buscar-enviados')
    return response.data
  },

  async buscarRecebidos() {
    const response = await http.get('convites/buscar-recebidos')
    return response.data
  },

  async removerConvite(id) {
    const response = await http.delete(`convites/remover-convite/${id}`)
    return response.data
  },

  async responderConvite(id, resposta) {
    console.log(id)
    const response = await http.post(
      `convites/responder-convite/${id}/${resposta}`
    )
    return response.data
  }
}
