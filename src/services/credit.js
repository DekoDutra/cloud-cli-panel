import { http } from './config'

export default {
  async listarHistorico() {
    const response = await http.get(
      'status-pagamento/buscar-pagamentos-usuario'
    )
    return response.data
  }
}
