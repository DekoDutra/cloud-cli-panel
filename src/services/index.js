import userService from '@/services/user'
import walletService from '@/services/wallet'
import adressService from '@/services/adress'
import phoneService from '@/services/phone'
import creditService from '@/services/credit'
import lembreteService from '@/services/lembrete'
import equipeService from '@/services/teams'
import twigService from '@/services/twig'
import conviteService from '@/services/convite'
import leaveService from '@/services/leaves'
import leafPlanService from '@/services/plans/leafs'
import serversService from '@/services/servers'
import logsService from '@/services/log'
import postgresService from '@/services/postgres'
import postgresPlanService from '@/services/plans/postgres'
import mysqlService from '@/services/mysql'
import mysqlPlanService from '@/services/plans/mysql'
import lakeService from '@/services/lakes'
import paymentService from '@/services/payment'
import ticketService from '@/services/tickets'

export default {
  userService,
  walletService,
  adressService,
  phoneService,
  creditService,
  lembreteService,
  equipeService,
  twigService,
  conviteService,
  leaveService,
  leafPlanService,
  serversService,
  logsService,
  postgresService,
  postgresPlanService,
  mysqlService,
  mysqlPlanService,
  lakeService,
  paymentService,
  ticketService
}
