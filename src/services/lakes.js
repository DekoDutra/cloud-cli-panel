import { http } from './config'

export default {
  async adicionarEquipeLake({ idLake, idEquipe }) {
    //feito
    const response = await http.post(
      `/lake/adicionar-equipe-lake/${idLake}/${idEquipe}`
    )
    return response.data
  },

  async adicionarUsuarioLake({ id, usuario }) {
    //feito
    const response = await http.post(
      `/lake/adicionar-usuario-lake/${usuario}/${id}`
    )
    return response.data
  },

  async alterarOwner({ id, usuario }) {
    // feito
    const response = await http.post(`/lake/alterar-owner/${usuario}/${id}`)
    return response.data
  },

  async atualizarLake({ idLake, lake }) {
    // feito
    const response = await http.put(`lake/atualizar-lake/${idLake}`, lake)
    return response.data
  },

  async buscarPorId(idLake) {
    //feito
    const response = await http.get(`lake/buscar-por-id/${idLake}`)
    return response.data
  },

  async deletarLake(idLake) {
    //feito
    const response = await http.post(`lake/deletar-lake/${idLake}`)
    return response.data
  },

  async downgradeLimiteLake({ id, limite }) {
    //feito
    const response = await http.put(`lake/downgrade-limite/${id}/${limite}`)
    return response.data
  },

  async upgradeLimiteLake({ id, limite }) {
    //feito
    const response = await http.put(`lake/upgrade-limite/${id}/${limite}`)
    return response.data
  },

  async gerarAPIKey(idLake) {
    //feito
    const response = await http.post(`lake/gerar-apikey/${idLake}`)
    return response.data
  },

  async listarEquipesLake(idLake) {
    //feito
    const response = await http.get(`lake/listar-equipes-lake/${idLake}`)
    return response.data
  },

  async listarItens(nome) {
    //feito
    const response = await http.get(`lake/listar-itens/${nome}`)
    return response.data
  },

  async listarLakesEquipe(idEquipe) {
    //feito
    const response = await http.get(`lake/listar-lakes-equipe/${idEquipe}`)
    return response.data
  },

  async listarLakesUsuario() {
    //feito
    const response = await http.get(`lake/listar-lakes-usuario`)
    return response.data
  },

  async listarUsuariosLake(idLake) {
    //feito
    const response = await http.get(`lake/listar-usuarios-lake/${idLake}`)
    return response.data
  },

  async novoLakeEquipe({ idEquipe, lake }) {
    //feito
    const response = await http.post(`lake/novo-lake-equipe/${idEquipe}`, lake)
    return response.data
  },

  async novoLakeUsuario(lake) {
    //feito
    const response = await http.post(`lake/novo-lake-usuario`, lake)
    return response.data
  },

  async removerEquipeLake({ idLake, idEquipe }) {
    //feito
    const response = await http.post(
      `lake/remover-equipe-lake/${idLake}/${idEquipe}`
    )
    return response.data
  },

  async removerItem({ idLake, idItem }) {
    //feito
    const response = await http.post(`lake/remover-item/${idLake}/${idItem}`)
    return response.data
  },

  async removerUsuarioLake({ id, usuario }) {
    //feito
    const response = await http.post(
      `lake/remover-usuario-lake/${usuario}/${id}`
    )
    return response.data
  },

  async transferirItem({ idLake, idItem, APIKey }) {
    //feito
    const response = await http.post(
      `lake/remover-equipe-lake/${idLake}/${idItem}/${APIKey}`
    )
    return response.data
  },

  async listarUso() {
    const response = await http.get(`lake/listar-uso-lakes`)
    return response.data
  }
}
