import { http } from './config'

export default {
  async novaLeaf(idTwig, plano, leaf) {
    //FEITO
    const response = await http.post(
      `leaves/nova-leaf/${idTwig}/${plano}`,
      leaf
    )
    return response.data
  },

  async buscarLeafPorId(id) {
    //FEITO
    const response = await http.get(`leaves/buscar-leaf-por-id/${id}`)
    return response.data
  },

  async deletarLeaf(id) {
    //FEITO
    const response = await http.delete(`leaves/deletar-leaf/${id}`)
    return response.data
  },

  async ativarManutencao(id) {
    //FEITO
    const response = await http.post(`leaves/ativar-manutencao/${id}`)
    return response.data
  },

  async desativarManutencao(id) {
    //FEITO
    const response = await http.post(`leaves/desativar-manutencao/${id}`)
    return response.data
  },

  async gerarGitkey(id) {
    //FEITO
    const response = await http.post(`leaves/gerar-gitkey/${id}`)
    return response.data
  },

  async habilitarSsl(id) {
    //FEITO
    const response = await http.post(`leaves/habilitar-ssl/${id}`)
    return response.data
  },

  async limitarLeaf({ idLeaf, memoria, cpu }) {
    //FEITO
    const response = await http.put(
      `leaves/habilitar-ssl/${idLeaf}/${memoria}/${cpu}`
    )
    return response.data
  },

  async listarLeavesTwig(idTwig) {
    //FEITO
    const response = await http.get(`leaves/listar-leaves-twig/${idTwig}`)
    return response.data
  },

  async mudarLeafDeTree({ id, tree }) {
    //FEITO
    const response = await http.put(`leaves/mudar-leaf-de-tree/${id}/${tree}`)
    return response.data
  },

  async mudarLeafDeTwig({ id, twig }) {
    //FEITO
    const response = await http.put(`leaves/mudar-leaf-de-twig/${id}/${twig}`)
    return response.data
  },

  async renomearLeaf({ id, nome }) {
    //FEITO
    const response = await http.put(`leaves/renomear-leaf/${id}/${nome}`)
    return response.data
  },

  async scaleLeaf({ id, web }) {
    //FEITO
    const response = await http.put(`leaves/scale-leaf/${id}/${web}`)
    return response.data
  },

  async startLeaf(id) {
    //FEITO
    const response = await http.post(`leaves/start-leaf/${id}`)
    return response.data
  },

  async stopLeaf(id) {
    //FEITO
    const response = await http.post(`leaves/stop-leaf/${id}`)
    return response.data
  },

  async visualizarGitkey(id) {
    //FEITO
    const response = await http.get(`leaves/visualizar-gitkey/${id}`)
    return response.data
  },
  async rebuildLeaf(id) {
    const response = await http.post(`leaves/rebuild-leaf/${id}`)
    return response.data
  },
  async desabilitarSSL(id) {
    const response = await http.post(`leaves/desabilitar-ssl/${id}`)
    return response.data
  },

  async alterarPlano({ id, plano }) {
    const response = await http.put(`leaves/trocar-plano/${id}/${plano}`)
    return response.data
  },

  async listarCommits(id) {
    const response = await http.get(`leaves/listar-commits/${id}/10`)
    return response.data
  },

  async contarAtivas() {
    const response = await http.get(`leaves/leaves-ativas-usuario`)
    return response.data
  }
}
