import { http } from './config'

export default {
  async listarLembretesNovos() {
    const response = await http.get('lembretes/listar-lembretes-novos')
    return response.data
  },

  async listarLembretes() {
    const response = await http.get('lembretes/listar-lembretes-usuario')
    return response.data
  }
}
