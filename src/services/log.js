import { http } from './config'

export default {
  async listarLogsCarteira() {
    const response = await http.get('logs/carteira')
    return response.data
  },
  async listarLogsEquipe(equipe) {
    const response = await http.get(`logs/equipe/${equipe}`)
    return response.data
  },
  async listarLogsTwig(twig) {
    const response = await http.get(`logs/twig/${twig}`)
    return response.data
  },
  async listarLogsLeaf(leaf) {
    const response = await http.get(`logs/leaf/${leaf}`)
    return response.data
  }
}
