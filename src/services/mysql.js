import { http } from './config'

export default {
  async listarAddonsUsuario(twig) {
    const response = await http.get(`addon/mysql/listar-addons-usuario/${twig}`)
    return response.data
  },

  async listarDbsAddon(role) {
    const response = await http.get(`addon/mysql/listar-addons-role/${role}`)
    return response.data
  },

  async adicionarAddonMySQL({ twig, servidor, plano }) {
    const response = await http.post(
      `addon/mysql/salvar-addon/${twig}/${servidor}/${plano}`
    )
    return response.data
  },

  async buscarMysqlAddon(id) {
    const response = await http.get(`addon/mysql/buscar-addon/${id}`)
    return response.data
  },

  async adicionarDB(id) {
    const response = await http.post(`addon/mysql/add-db/${id}`)
    return response.data
  },

  async removerDB(payload) {
    const response = await http.delete(`addon/mysql/deletar-db/${
      payload.nome
    }/${payload.role}
    `)
    return response.data
  },

  async removerAddon(nome) {
    const response = await http.delete(`addon/mysql/deletar-addon/${nome}
    `)
    return response.data
  },

  async mudarPlano(payload) {
    const response = await http.put(
      `addon/mysql/mudar-plano/${payload.addon}/${payload.plano}`
    )
    return response.data
  }
}
