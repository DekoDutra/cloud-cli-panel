import { http } from './config'

export default {
  async novoTelefone(phone) {
    const response = await http.post('telefones/novo-telefone', phone)
    return response.data
  },

  async buscarTelefonePorId(id) {
    const response = await http.get(`telefones/buscar-telefone-por-id/${id}`)
    return response.data
  },

  async atualizarTelefone(phone) {
    const response = await http.put(
      `telefones/atualizar-telefone/${phone.id}`,
      phone
    )
    return response.data
  },

  async deletarTelefone(id) {
    const response = await http.delete(`telefones/deletar-telefone/${id}`)
    return response.data
  },

  async listarTelefonesUsuario() {
    const response = await http.get('telefones/listar-telefones-usuario')
    return response.data
  },

  async alterarTelefoneDefault(id) {
    const response = await http.put(`telefones/alterar-default/${id}`)
    return response.data
  }
}
