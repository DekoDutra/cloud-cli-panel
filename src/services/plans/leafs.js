import { http } from '../config'

export default {
  async getTodosPlanos() {
    const response = await http.get('planos/buscar-todos')
    return response.data
  }
}
