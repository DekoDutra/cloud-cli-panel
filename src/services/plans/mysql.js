import { http } from '../config'

export default {
  async listarPlanosMySQL() {
    const response = await http.get('planos/database/mysql')
    return response.data
  }
}
