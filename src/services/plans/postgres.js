import { http } from '../config'

export default {
  async listarPlanosPostgres() {
    const response = await http.get('planos/database/postgres')
    return response.data
  }
}
