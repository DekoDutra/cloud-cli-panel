import { http } from './config'

export default {
  async listarAddonsUsuario(twig) {
    const response = await http.get(`addon/postgre/listar-addons/${twig}`)
    return response.data
  },

  async buscarAddonPostgre(nome) {
    const response = await http.get(`addon/postgre/buscar-addon/${nome}`)
    return response.data
  },

  async adicionarAddonPostgre({ twig, servidor, plano }) {
    const response = await http.post(
      `addon/postgre/salvar-addon/${twig}/${servidor}/${plano}`
    )
    return response.data
  },

  async alterarPlanoPostgre({ addon, plano }) {
    const response = await http.post(
      `addon/postgre/mudar-plano/${addon}/${plano}`
    )
    return response.data
  },

  async deletarAddonPostgre(id) {
    const response = await http.delete(`addon/postgre/deletar-addon/${id}`)
    return response.data
  }
}
