import { http } from './config'

export default {
  async listarServidores() {
    const response = await http.get('servidores/listar-servidores')
    return response.data
  }
}
