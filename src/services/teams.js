import { http } from './config'

export default {
  async novaEquipe(team) {
    const response = await http.post('equipes/nova-equipe', team)
    return response.data
  },

  async listarEquipesUsuario() {
    const response = await http.get('equipes/listar-equipes-usuario')
    return response.data
  },

  async buscarEquipePorId(id) {
    const response = await http.get(`equipes/buscar-equipe-por-id/${id}`)
    return response.data
  },

  async deletarEquipe(id) {
    const response = await http.delete(`equipes/deletar-equipe/${id}`)
    return response.data
  },

  async atualizarEquipe(team) {
    const response = await http.put(`equipes/atualizar-equipe/${team.id}`, team)
    return response.data
  },

  async listarMembrosEquipe(id) {
    const response = await http.get(`equipes/listar-membros-equipe/${id}`)
    return response.data
  },

  async adicionarMembro(idEquipe, idUsuario, acesso) {
    const response = await http.post(
      `equipes/adicionar-membro/${idUsuario}/${idEquipe}/${acesso}`
    )
    return response.data
  },

  async removerMembro(idEquipe, idUsuario) {
    const response = await http.post(
      `equipes/remover-membro/${idUsuario}/${idEquipe}`,
      {}
    )
    return response.data
  },

  async alterarNivelMembro(idEquipe, idUsuario, nivelacesso) {
    const response = await http.put(
      `equipes/alterar-nivel-membro/${idUsuario}/${idEquipe}/${nivelacesso}`,
      {}
    )
    return response.data
  },

  async passarLideranca(idEquipe, idUsuario) {
    const response = await http.post(
      `equipes/passar-lideranca/${idUsuario}/${idEquipe}`,
      {}
    )
    return response.data
  },

  async sairEquipe(idEquipe) {
    const response = await http.post(`equipes/sair-equipe/${idEquipe}`, {})
    return response.data
  },

  async buscarEquipePorNome(buscador, token) {
    console.log(buscador)
    const response = await http.get(
      `equipes/buscar-equipe-por-nome/${buscador}`,
      { headers: { Authorization: token } }
    )
    return response.data
  }
}
