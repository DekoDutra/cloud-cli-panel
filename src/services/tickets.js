import { http } from './config'

export default {
  async listarTickets() {
    const response = await http.get('tickets/listar-tickets-usuario')
    return response.data
  },

  async novoTicket(payload) {
    const response = await http.post('tickets/novo-ticket', payload)
    return response.data
  },

  async novaResposta(payload) {
    const response = await http.post(
      `tickets/nova-resposta/${payload.ticket}/${payload.resposta}`
    )
    return response.data
  },

  async listarRespostas(id) {
    const response = await http.get(`tickets/listar-respostas-ticket/${id}`)
    return response.data
  },

  async removerResposta(id) {
    const response = await http.delete(`tickets/remover-resposta/${id}`)
    return response.data
  },

  async fecharTicket(id) {
    const response = await http.put(`tickets/fechar-ticket/${id}`)
    return response.data
  },

  async buscarTicket(id) {
    const response = await http.get(`tickets/buscar-ticket/${id}`)
    return response
  }
}
