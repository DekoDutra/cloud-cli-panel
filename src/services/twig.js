import { http } from './config'

export default {
  async novoTwig(twig) {
    const response = await http.post('twigs/novo-twig-usuario', twig)
    return response.data
  },

  async listarTwigsUsuario() {
    const response = await http.get('twigs/listar-twigs-usuario')
    return response.data
  },

  async deletarTwig(id) {
    const response = await http.delete(`twigs/deletar-twig/${id}`)
    return response.data
  },

  async buscarTwigPorId(id) {
    const response = await http.get(`twigs/buscar-twig-por-id/${id}`)
    return response.data
  },

  async atualizarTwig(twig) {
    const response = await http.put(`twigs/atualizar-twig/${twig.id}`, twig)
    return response.data
  },

  async listarTwigsEquipe(id) {
    const response = await http.get(`twigs/listar-twigs-equipe/${id}`)
    return response.data
  },

  async adicionarMembroTwig({ idEquipe, idRamo, idUsuario, nivelacesso }) {
    //feito
    const response = await http.post(
      `twigs/adicionar-membro-twig/${idRamo}/${idEquipe}/${idUsuario}/${nivelacesso}`
    )
    return response.data
  },

  async adicionarNovoUsuarioTwig(idTwig, idUsuario, nivelacesso) {
    const response = await http.post(
      `twigs/adicionar-usuario-twig/${idUsuario}/${idTwig}/${nivelacesso}`
    )
    return response.data
  },

  async alterarFinanciardor({ twig, usuario }) {
    const response = await http.post(
      `twigs/alterar-financiador/${usuario}/${twig}`
    )
    return response.data
  },

  async alterarNivelMembro({ idTwig, idUsuario, idEquipe, nivelacesso }) {
    const response = await http.post(
      `twigs/adicionar-usuario-twig/${idTwig}/${idUsuario}/${idEquipe}/${nivelacesso}`
    )
    return response.data
  },

  async alterarNivelUsuario({ id, idUsuario, nivel }) {
    const response = await http.post(
      `twigs/alterar-nivel-usuario/${idUsuario}/${id}/${nivel}`
    )
    return response.data.usuario
  },

  async listarUsuariosTwig(idTwig) {
    const response = await http.get(`twigs/listar-usuarios-twig/${idTwig}`)
    return response.data
  },

  async novoTwigEquipe(equipe, twig) {
    const response = await http.post(`twigs/novo-twig-equipe/${equipe}`, twig)
    return response.data
  },

  async listarEquipesTwig(idTwig) {
    const response = await http.get(`twigs/listar-equipes-twig/${idTwig}`)
    return response.data
  },

  async removerUsuarioTwig({ usuario, twig }) {
    const response = await http.delete(
      `twigs/remover-usuario-twig/${usuario}/${twig}`
    )
    return response.data
  },

  async removerEquipeTwig({ equipe, twig }) {
    const response = await http.delete(
      `twigs/remover-equipe-twig/${equipe}/${twig}`
    )
    return response.data
  },

  async adicionarEquipeTwig({ equipe, twig }) {
    const response = await http.post(
      `twigs/adicionar-equipe-twig/${equipe}/${twig}`
    )
    return response.data
  }
}
