import { http, parseJwt } from './config'

export default {
  async login(user) {
    const response = await http.post('publico/login', user)
    return response.data
  },

  async registrarUsuario(user) {
    const response = await http.post('publico/registrar-usuario', user)
    return response.data
  },

  async buscarUsuarioPorId(id, token) {
    const response = await http.get(`usuarios/buscar-usuario-por-id/${id}`, {
      headers: { Authorization: token }
    })
    return response.data
  },

  async _buscarMeuUsuario() {
    const response = await http.get(`usuarios/buscar-meu-usuario`)
    return response.data
  },

  async atualizarUsuario(id, token) {
    const response = await http.get(`usuarios/atualizar-usuario/${id}`, {
      headers: { Authorization: token }
    })
    return response.data
  },

  async _atualizarMeuUsuario(token, payload) {
    const response = await http.put(
      `usuarios/atualizar-usuario/${parseJwt(token).id_usuario}`,
      payload,
      { headers: { Authorization: token } }
    )
    return response.data
  },

  async buscarUsuarioNomeEmail(val, token) {
    const response = await http.get(
      `usuarios/buscar-usuario-nome-email/${val}`,
      { headers: { Authorization: token } }
    )
    return response.data
  },

  async alterarSenha({ novo, antigo }) {
    const response = await http.put(`usuarios/alterar-senha/${novo}/${antigo}`)
    return response.data
  }
}
