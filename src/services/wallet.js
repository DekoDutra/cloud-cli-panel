import { http } from './config'

export default {
  async buscarCarteira() {
    const response = await http.get('carteira/buscar-carteira')
    return response.data
  },
  async pagamentoRede(payload) {
    const response = await http.post('carteira/rede/pagar-cartao', payload)
    return response.data
  },
  async pagamentoCartaoPagarme(payload) {
    const response = await http.post('carteira/pagarme/pagar-cartao', payload)
    return response.data
  },
  async pagamentoBoletoPagarme(saldo) {
    const response = await http.post(`carteira/pagarme/pagar-boleto/${saldo}`)
    return response.data
  },
  async buscarHistorico() {
    const response = await http.get(
      'status-pagamento/buscar-pagamentos-usuario'
    )
    return response.data
  },
  async buscarBoletos() {
    const response = await http.get('status-pagamento/buscar-boletos')
    return response.data
  }
}
