export function process(data) {
  if (data instanceof Object) {
    if (data.cod >= 400 || data.status >= 400) throw data
    return data
  } else throw 'The returned data must be an object type'
}

export const loading = (dispatch, bool) => {
  dispatch('loading/setLoading', bool, { root: true })
}
