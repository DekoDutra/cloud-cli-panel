import service from '@/services/adress'
import { process } from '../common'

const state = {
  adresses: [],
  adress: {}
}

const getters = {
  adresses: state => state.adresses,
  adress: state => state.adress
}

const actions = {
  async getAdresses({ dispatch, commit }) {
    try {
      let result = process(await service.listarEnderecosUsuario())
      commit('SET_ADRESSES', result.data)
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  },

  newAdress({ dispatch }, adress) {
    return new Promise(async resolve => {
      try {
        let result = process(await service.novoEndereco(adress))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('getAdresses')
        resolve()
      } catch (error) {
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  delAdress({ dispatch }, id) {
    return new Promise(async resolve => {
      try {
        let result = process(await service.deletarEndereco(id))
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getAdresses')
        resolve()
      } catch (error) {
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async editAdress({ dispatch }, adress) {
    try {
      let result = process(await service.atualizarEndereco(adress))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getAdresses')
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  },

  defaulterAdress({ dispatch }, id) {
    return new Promise(async resolve => {
      try {
        let result = process(await service.alterarDefaultEndereco(id))
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getAdresses')
        resolve()
      } catch (error) {
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  }
}

const mutations = {
  SET_ADRESSES(state, adresses) {
    state.adresses = adresses
  },

  SET_ADRESS(state, adress) {
    state.adress = adress
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
