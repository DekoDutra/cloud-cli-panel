import service from '@/services/convite'
import { process, loading } from '../common'

const state = {
  sendedInvite: [],
  recievedInvite: []
}

const getters = {
  sendedInvite: state => state.sendedInvite,
  recievedInvite: state => state.recievedInvite
}

const actions = {
  async getSendedInvites({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.buscarEnviados())
      commit('SET_SENDED_INVITES', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getRecievedInvites({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.buscarRecebidos())
      commit('SET_RECIEVED_INVITES', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  removeInvite({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.removerConvite(id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getSendedInvites')
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  async answerInvite({ dispatch }, invite) {
    loading(dispatch, true)

    try {
      let result = process(
        await service.responderConvite(invite.id, invite.answer)
      )
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getRecievedInvites')
      dispatch('getSendedInvites')
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_SENDED_INVITES(state, sendedInvite) {
    state.sendedInvite = sendedInvite
  },

  SET_RECIEVED_INVITES(state, recievedInvite) {
    state.recievedInvite = recievedInvite
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
