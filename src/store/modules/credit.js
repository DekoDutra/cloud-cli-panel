import service from '@/services/credit'
import { process } from '../common'

const state = {
  credits: [],
  credit: {}
}

const getters = {
  credits: state => state.credits,
  credit: state => state.credit
}

const actions = {
  async getHistoricCredit({ dispatch, commit }) {
    try {
      let result = process(await service.listarHistorico())
      commit('SET_CREDITS', result.data)
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_CREDITS(state, credits) {
    state.credits = credits
  },

  SET_CREDIT(state, credit) {
    state.credit = credit
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
