import user from './user'
import message from './message'
import wallet from './wallet'
import adress from './adress'
import phone from './phone'
import credit from './credit'
import lembrete from './lembrete'
import teams from './teams'
import loading from './loading'
import twig from './twig'
import convite from './convite'
import leaves from './leaves'
import leafPlan from './plans/leafs'
import servers from './servers'
import logs from './logs'
import tabs from './tabs'
import postgres from './postgres'
import postgresPlan from './plans/postgres'
import mysql from './mysql'
import mysqlPlan from './plans/mysql'
import lake from './lake'
import payment from './payment'
import tickets from './tickets'

export default {
  user,
  message,
  wallet,
  adress,
  phone,
  credit,
  lembrete,
  teams,
  loading,
  twig,
  convite,
  leaves,
  leafPlan,
  servers,
  logs,
  tabs,
  postgres,
  postgresPlan,
  mysql,
  mysqlPlan,
  lake,
  payment,
  tickets
}
