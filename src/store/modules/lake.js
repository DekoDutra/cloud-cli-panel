import service from '@/services/lakes'
import { process, loading } from '../common'

const state = {
  lake: {},
  teamsLake: [],
  itens: [],
  lakes: [],
  usersLake: [],
  uso: ''
}

const getters = {
  lake: state => state.lake,
  teamsLake: state => state.teamsLake,
  itens: state => state.itens,
  lakes: state => state.lakes,
  usersLake: state => state.usersLake,
  uso: state => state.uso
}

const actions = {
  async newUserLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.novoLakeUsuario(payload))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getLakesUser')
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async newTeamLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.novoLakeEquipe(payload))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async updateLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.atualizarLake(payload))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  deleteLake({ dispatch }, idLake) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.deletarLake(idLake))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLakesUser')
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  removeTeamLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.removerEquipeLake(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  removeItem({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.removerItem(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLake', payload.idLake)
        dispatch('getItensLake', payload.nome)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  removeUserLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.removerUsuarioLake(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLake', payload.id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  async addTeamLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.adicionarEquipeLake(payload))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async addUserLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.adicionarUsuarioLake(payload))
      loading(dispatch, false)
      dispatch('getUsersLake', payload.id)
      dispatch('message/sendSuccess', result, { root: true })
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getLake({ dispatch, commit }, idLake) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.buscarPorId(idLake))
      loading(dispatch, false)
      commit('SET_LAKE', result)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getTeamsLake({ dispatch, commit }, idLake) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.listarEquipesLake(idLake))
      loading(dispatch, false)
      commit('SET_TEAMS_LAKE', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getItensLake({ dispatch, commit }, nome) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.listarItens(nome))
      loading(dispatch, false)
      commit('SET_ITENS', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getLakesTeam({ dispatch, commit }, idTeam) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.listarLakesEquipe(idTeam))
      loading(dispatch, false)
      commit('SET_LAKES', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getLakesUser({ dispatch, commit }) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.listarLakesUsuario())
      loading(dispatch, false)
      commit('SET_LAKES', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getUsersLake({ dispatch, commit }, idLake) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.listarUsuariosLake(idLake))
      loading(dispatch, false)
      commit('SET_USERS_LAKE', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  changeOwner({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.alterarOwner(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  downgradeLimiteLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.downgradeLimiteLake(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLake', payload.id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  upgradeLimiteLake({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.upgradeLimiteLake(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLake', payload.id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  generateAPIKey({ dispatch }, idLake) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.gerarAPIKey(idLake))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLake', idLake)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  transferItem({ dispatch }, payload) {
    //feito
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.transferirItem(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  async getUso({ dispatch, commit }) {
    //feito
    loading(dispatch, true)

    try {
      let result = process(await service.listarUso())
      loading(dispatch, false)
      commit('SET_USO', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_LAKE(state, lake) {
    state.lake = lake
  },

  SET_TEAMS_LAKE(state, teamsLake) {
    state.teamsLake = teamsLake
  },

  SET_ITENS(state, itens) {
    state.itens = itens
  },

  SET_LAKES(state, lakes) {
    state.lakes = lakes
  },

  SET_USERS_LAKE(state, usersLake) {
    state.usersLake = usersLake
  },

  SET_USO(state, uso) {
    state.uso = uso
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
