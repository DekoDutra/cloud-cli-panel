import service from '@/services/leaves'
import { process, loading } from '../common'

const state = {
  leaves: [],
  leaf: {},
  commits: [],
  ativas: ''
}

const getters = {
  leaves: state => state.leaves,
  leaf: state => state.leaf,
  commits: state => state.commits,
  ativas: state => state.ativas
}

const actions = {
  async newLeaf({ dispatch }, leaf) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(
          await service.novaLeaf(leaf.twig, leaf.plano, leaf.content)
        )
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('getLeavesTwig', leaf.twig)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async getLeavesTwig({ dispatch, commit }, idTwig) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarLeavesTwig(idTwig))
      commit('SET_LEAVES', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getLeaf({ dispatch, commit }, id) {
    loading(dispatch, true)

    try {
      let result = process(await service.buscarLeafPorId(id))
      commit('SET_LEAF', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  deleteLeaf({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.deletarLeaf(payload.id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeavesTwig', payload.twig)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  deleteLeafTwig({ dispatch }, leaf) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.deletarLeaf(leaf.id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeavesTwig', leaf.twig)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  activeMaintenanceTwig({ dispatch }, leaf) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.ativarManutencao(leaf.id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeavesTwig', leaf.twig)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  desactiveMaintenanceTwig({ dispatch }, leaf) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.desativarManutencao(leaf.id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeavesTwig', leaf.twig)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  activeMaintenance({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.ativarManutencao(id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeaf', id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  desactiveMaintenance({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.desativarManutencao(id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeaf', id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  async activeSSL({ dispatch }, id) {
    loading(dispatch, true)

    try {
      let result = process(await service.habilitarSsl(id))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getLeaf', id)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  limitLeaf({ dispatch }, leaf) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.limitarLeaf(leaf))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  scaleLeaf({ dispatch }, leaf) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.scaleLeaf(leaf))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeaf', leaf.id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  async startLeafTwig({ dispatch }, leaf) {
    loading(dispatch, true)

    try {
      let result = process(await service.startLeaf(leaf.id))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getLeavesTwig', leaf.twig)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async startLeaf({ dispatch }, id) {
    loading(dispatch, true)

    try {
      let result = process(await service.startLeaf(id))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getLeaf', id)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async stopLeaf({ dispatch }, id) {
    loading(dispatch, true)

    try {
      let result = process(await service.stopLeaf(id))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getLeaf', id)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async stopLeafTwig({ dispatch }, leaf) {
    loading(dispatch, true)

    try {
      let result = process(await service.stopLeaf(leaf.id))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getLeavesTwig', leaf.twig)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  changeTree({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.mudarLeafDeTree(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  changeTwig({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.mudarLeafDeTwig(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  generateGitkey({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.gerarGitkey(id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeaf', id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  rebuildLeaf({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.rebuildLeaf(id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeaf', id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  desabilitarSSL({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.desabilitarSSL(id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeaf', id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  alterarPlano({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.alterarPlano(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeaf', payload.id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  renomearLeaf({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.renomearLeaf(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getLeaf', payload.id)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },
  async getCommits({ dispatch, commit }, id) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarCommits(id))
      commit('SET_COMMITS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },
  async getAtivas({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.contarAtivas())
      commit('SET_ATIVAS', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_LEAVES(state, leaves) {
    state.leaves = leaves
  },

  SET_LEAF(state, leaf) {
    state.leaf = leaf
  },

  SET_COMMITS(state, commits) {
    state.commits = commits
  },

  SET_ATIVAS(state, ativas) {
    state.ativas = ativas
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
