import service from '@/services/lembrete'
import { process } from '../common'

const state = {
  lembretesNovos: [],
  lembretes: []
}

const getters = {
  lembretes: state => state.lembretes,
  lembretesNovos: state => state.lembretesNovos
}

const actions = {
  async getNewLembretes({ dispatch, commit }) {
    try {
      let result = process(await service.listarLembretesNovos())
      commit('SET_LEMBRETESNOVOS', result.data)
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getLembretes({ dispatch, commit }) {
    try {
      let result = process(await service.listarLembretes())
      commit('SET_LEMBRETES', result.data)
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_LEMBRETESNOVOS(state, lembretesNovos) {
    state.lembretesNovos = lembretesNovos
  },

  SET_LEMBRETES(state, lembretes) {
    state.lembretes = lembretes
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
