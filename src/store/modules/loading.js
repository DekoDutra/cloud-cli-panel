const state = {
  loading: true
}

const getters = {
  loading: state => state.loading
}

const actions = {
  setLoading({ commit }, loading) {
    commit('SET_LOADING', loading)
  }
}

const mutations = {
  SET_LOADING(state, loading) {
    state.loading = loading
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
