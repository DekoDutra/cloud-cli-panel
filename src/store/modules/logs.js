import service from '@/services/log'
import { loading, process } from '../common'

const state = {
  logs: []
}

const getters = {
  logs: state => state.logs
}

const actions = {
  async listarLogsCarteira({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarLogsCarteira())
      commit('SET_LOGS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async listarLogsEquipe({ dispatch, commit }, equipe) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarLogsEquipe(equipe))
      commit('SET_LOGS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async listarLogsTwig({ dispatch, commit }, twig) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarLogsTwig(twig))
      commit('SET_LOGS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async listarLogsLeaf({ dispatch, commit }, leaf) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarLogsLeaf(leaf))
      commit('SET_LOGS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_LOGS(state, logs) {
    state.logs = logs
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
