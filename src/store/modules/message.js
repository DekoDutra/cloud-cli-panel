const state = {
  error: {},
  success: {}
}

const getters = {
  error: state => state.error,
  success: state => state.success
}

const actions = {
  sendError({ commit }, error) {
    if (error instanceof Object && Object.entries(error).length > 0) {
      commit('SET_ERROR', error)
    }
  },

  sendSuccess({ commit }, success) {
    if (success instanceof Object && Object.entries(success).length > 0) {
      commit('SET_SUCCESS', success)
    }
  }
}

const mutations = {
  SET_ERROR(state, error) {
    state.error = error
  },

  SET_SUCCESS(state, success) {
    state.success = success
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
