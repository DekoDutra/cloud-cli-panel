import service from '@/services/mysql'
import { process, loading } from '../common'

const state = {
  mysqls: [],
  mysql: {},
  dbs: []
}

const getters = {
  mysqls: state => state.mysqls,
  mysql: state => state.mysql,
  dbs: state => state.dbs
}

const actions = {
  async newAddon({ dispatch }, addon) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.adicionarAddonMySQL(addon))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('listarAddonsUsuario', addon.twig)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async listarAddonsUsuario({ dispatch, commit }, twig) {
    try {
      let result = process(await service.listarAddonsUsuario(twig))
      commit('SET_MYSQLS', result.data)
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  },

  async buscarMysqlAddon({ dispatch, commit }, id) {
    try {
      let result = process(await service.buscarMysqlAddon(id))
      commit('SET_MYSQL', result.data)
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  },

  async listarDbsAddon({ dispatch, commit }, nome) {
    try {
      let result = process(await service.listarDbsAddon(nome))
      commit('SET_DBS', result.data)
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  },

  async newDbAddon({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.adicionarDB(payload.id))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('listarDbsAddon', payload.role)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async removeDB({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.removerDB(payload))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('listarDbsAddon', payload.role)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async removeAddon({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.removerAddon(payload.addon))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('listarAddonsUsuario', payload.twig)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async alterarPlano({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.mudarPlano(payload))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('buscarMysqlAddon', payload.addon)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  }
}

const mutations = {
  SET_MYSQLS(state, mysqls) {
    state.mysqls = mysqls
  },

  SET_MYSQL(state, mysql) {
    state.mysql = mysql
  },

  SET_DBS(state, dbs) {
    state.dbs = dbs
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
