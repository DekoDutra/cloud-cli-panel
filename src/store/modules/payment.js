import service from '@/services/payment'
import { loading, process } from '../common'

const state = {
  payments: []
}

const getters = {
  payments: state => state.payments
}

const actions = {
  async getPayments({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.buscarHistorico())
      commit('SET_PAYMENTS', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_PAYMENTS(state, payments) {
    state.payments = payments
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
