import service from '@/services/phone'
import { loading, process } from '../common'

const state = {
  phones: [],
  phone: {}
}

const getters = {
  phones: state => state.phones,
  phone: state => state.phone
}

const actions = {
  async getPhones({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarTelefonesUsuario())
      commit('SET_PHONES', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  newPhone({ dispatch }, phone) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.novoTelefone(phone))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('getPhones')
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  delPhone({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.deletarTelefone(id))
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getPhones')
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async editPhone({ dispatch }, phone) {
    loading(dispatch, true)

    try {
      let result = process(await service.atualizarTelefone(phone))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getPhones')
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  defaulterPhone({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.alterarTelefoneDefault(id))
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getPhones')
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  }
}

const mutations = {
  SET_PHONES(state, phones) {
    state.phones = phones
  },

  SET_PHONE(state, phone) {
    state.phone = phone
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
