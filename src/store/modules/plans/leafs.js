import service from '@/services/plans/leafs'
import { process, loading } from '../../common'

const state = {
  plans: [],
  plan: {}
}

const getters = {
  plans: state => state.plans,
  plan: state => state.plan
}

const actions = {
  async getAllPlans({ dispatch, commit }) {
    loading(dispatch, true)
    try {
      let result = process(await service.getTodosPlanos())
      loading(dispatch, false)
      commit('SET_PLANS', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_PLANS(state, plans) {
    state.plans = plans
  },

  SET_PLAN(state, plan) {
    state.plan = plan
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
