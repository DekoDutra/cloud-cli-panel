import service from '@/services/plans/mysql'
import { process, loading } from '../../common'

const state = {
  mysqlPlans: [],
  mysqlPlan: {}
}

const getters = {
  mysqlPlans: state => state.mysqlPlans,
  mysqlPlan: state => state.mysqlPlan
}

const actions = {
  async getmysqlPlans({ dispatch, commit }) {
    loading(dispatch, true)
    try {
      let result = process(await service.listarPlanosMySQL())
      loading(dispatch, false)
      commit('SET_MYSQL_PLANS', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_MYSQL_PLANS(state, mysqlPlans) {
    state.mysqlPlans = mysqlPlans
  },

  SET_MYSQL_PLAN(state, mysqlPlan) {
    state.mysqlPlan = mysqlPlan
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
