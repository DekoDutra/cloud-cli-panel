import service from '@/services/plans/postgres'
import { process, loading } from '../../common'

const state = {
  postgresPlans: [],
  postgresPlan: {}
}

const getters = {
  postgresPlans: state => state.postgresPlans,
  postgresPlan: state => state.postgresPlan
}

const actions = {
  async getPostgresPlans({ dispatch, commit }) {
    loading(dispatch, true)
    try {
      let result = process(await service.listarPlanosPostgres())
      loading(dispatch, false)
      commit('SET_POSTGRES_PLANS', result.data)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_POSTGRES_PLANS(state, postgresPlans) {
    state.postgresPlans = postgresPlans
  },

  SET_POSTGRES_PLAN(state, postgresPlan) {
    state.postgresPlan = postgresPlan
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
