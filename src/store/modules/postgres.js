import service from '@/services/postgres'
import { process, loading } from '../common'

const state = {
  postgres: [],
  postgre: {}
}

const getters = {
  postgres: state => state.postgres,
  postgre: state => state.postgre
}

const actions = {
  async newAddon({ dispatch }, addon) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.adicionarAddonPostgre(addon))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('listarAddonsUsuario', addon.twig)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async listarAddonsUsuario({ dispatch, commit }, twig) {
    try {
      let result = process(await service.listarAddonsUsuario(twig))
      commit('SET_POSTGRES', result.data)
    } catch (error) {
      dispatch('message/sendError', error, { root: true })
    }
  },

  async buscarAddonPostgre({ dispatch, commit }, nome) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.buscarAddonPostgre(nome))
        commit('SET_POSTGRE', result.data)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async alterarPlanoPostgre({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.buscarAddonPostgre(payload))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('buscarAddonPostgre', payload.addon)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async deletarAddonPostgre({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.deletarAddonPostgre(payload.addon))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('listarAddonsUsuario', payload.twig)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  }
}

const mutations = {
  SET_POSTGRES(state, postgres) {
    state.postgres = postgres
  },

  SET_POSTGRE(state, postgre) {
    state.postgre = postgre
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
