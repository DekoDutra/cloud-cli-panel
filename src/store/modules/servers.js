import service from '@/services/servers'
import { loading, process } from '../common'

const state = {
  servers: [],
  server: {}
}

const getters = {
  servers: state => state.servers,
  server: state => state.server
}

const actions = {
  async getServers({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarServidores())
      commit('SET_SERVERS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_SERVERS(state, servers) {
    state.servers = servers
  },

  SET_SERVER(state, server) {
    state.server = server
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
