const state = {
  teamTab: 0,
  twigTab: 0,
  lakeTab: 0,
  supTab: 0
}

const getters = {
  teamTab: state => state.teamTab,
  twigTab: state => state.twigTab,
  lakeTab: state => state.lakeTab,
  supTab: state => state.supTab
}

const actions = {
  setTeamTab({ commit }, value) {
    commit('SET_TEAM_TAB', value)
  },
  setTwigTab({ commit }, value) {
    commit('SET_TWIG_TAB', value)
  },
  setLakeTab({ commit }, value) {
    commit('SET_LAKE_TAB', value)
  },
  setSupTab({ commit }, value) {
    commit('SET_SUP_TAB', value)
  }
}

const mutations = {
  SET_TEAM_TAB(state, teamTab) {
    state.teamTab = teamTab
  },
  SET_TWIG_TAB(state, twigTab) {
    state.twigTab = twigTab
  },
  SET_LAKE_TAB(state, lakeTab) {
    state.lakeTab = lakeTab
  },
  SET_SUP_TAB(state, supTab) {
    state.supTab = supTab
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
