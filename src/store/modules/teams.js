import service from '@/services/teams'
import { loading, process } from '../common'

const state = {
  teams: [],
  team: {},
  members: []
}

const getters = {
  teams: state => state.teams,
  team: state => state.team,
  members: state => state.members
}

const actions = {
  async getTeams({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarEquipesUsuario())
      commit('SET_TEAMS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getTeam({ dispatch, commit }, id) {
    loading(dispatch, true)
    try {
      let result = process(await service.buscarEquipePorId(id))
      commit('SET_TEAM', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async newTeam({ dispatch }, body) {
    loading(dispatch, true)
    try {
      let result = process(await service.novaEquipe(body))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getTeams')
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async editTeam({ dispatch }, equipe) {
    loading(dispatch, true)
    try {
      let result = process(await service.atualizarEquipe(equipe))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getTeam', equipe.id)
      dispatch('getTeams')
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async delTeam({ dispatch }, id) {
    loading(dispatch, true)
    try {
      let result = process(await service.deletarEquipe(id))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getTeams')
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getMembers({ dispatch, commit }, id) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarMembrosEquipe(id))
      commit('SET_MEMBERS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async addMember({ dispatch }, membro) {
    loading(dispatch, true)
    try {
      let result = process(
        await service.adicionarMembro(membro.id, membro.idUsuario, membro.nivel)
      )
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getMembers', membro.id)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async removeMember({ dispatch }, data) {
    loading(dispatch, true)
    try {
      let result = process(
        await service.removerMembro(data.equipe, data.membro)
      )
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getMembers', data.equipe)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async alterarNivelMembro({ dispatch }, data) {
    loading(dispatch, true)
    console.log(data)
    try {
      let result = process(
        await service.alterarNivelMembro(data.id, data.idUsuario, data.nivel)
      )
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getMembers', data.id)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async passarLideranca({ dispatch }, data) {
    loading(dispatch, true)
    try {
      let result = process(
        await service.passarLideranca(data.equipe, data.membro)
      )
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getMembers', data.equipe)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async leaveTeam({ dispatch }, data) {
    loading(dispatch, true)
    try {
      let result = process(await service.sairEquipe(data.equipe))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getMembers', data.equipe)
      dispatch('getTeams')
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_TEAMS(state, teams) {
    state.teams = teams
  },
  SET_TEAM(state, team) {
    state.team = team
  },
  SET_MEMBERS(state, members) {
    state.members = members
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
