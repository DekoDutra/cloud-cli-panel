import service from '@/services/tickets'
import { loading, process } from '../common'

const state = {
  tickets: [],
  respostas: [],
  ticket: {}
}

const getters = {
  tickets: state => state.tickets,
  respostas: state => state.respostas,
  ticket: state => state.ticket
}

const actions = {
  async listarTickets({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarTickets())
      commit('SET_TICKETS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async listarRespostas({ dispatch, commit }, id) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarRespostas(id))
      commit('SET_RESPOSTAS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async buscarTicket({ dispatch, commit }, id) {
    loading(dispatch, true)

    try {
      let result = process(await service.buscarTicket(id))
      commit('SET_TICKET', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  novoTicket({ dispatch }, ticket) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.novoTicket(ticket))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('listarTickets')
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  novaResposta({ dispatch }, resposta) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.novaResposta(resposta))
        dispatch('message/sendSuccess', result.data, { root: true })
        dispatch('listarRespostas', resposta.ticket)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  removerResposta({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.removerResposta(payload.resposta))
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('listarRespostas', payload.ticket)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  fecharTicket({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.fecharTicket(id))
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('listarTickets')
        dispatch('buscarTicket', id)
        resolve()
        loading(dispatch, false)
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  }
}

const mutations = {
  SET_TICKETS(state, tickets) {
    state.tickets = tickets
  },
  SET_RESPOSTAS(state, respostas) {
    state.respostas = respostas
  },
  SET_TICKET(state, ticket) {
    state.ticket = ticket
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
