import service from '@/services/twig'
import { process, loading } from '../common'

const state = {
  twigs: [],
  twig: {},
  usersTwig: [],
  twigsEquipe: [],
  equipesTwig: []
}

const getters = {
  twigs: state => state.twigs,
  twig: state => state.twig,
  usersTwig: state => state.usersTwig,
  twigsEquipe: state => state.twigsEquipe,
  equipesTwig: state => state.equipesTwig
}

const actions = {
  async getTwigs({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarTwigsUsuario())
      commit('SET_TWIGS', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getTwigsEquipe({ dispatch, commit }, equipe) {
    loading(dispatch, true)
    try {
      let result = process(await service.listarTwigsEquipe(equipe))
      commit('SET_TWIGS_EQUIPE', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getEquipesTwig({ dispatch, commit }, twig) {
    loading(dispatch, true)
    try {
      let result = process(await service.listarEquipesTwig(twig))
      commit('SET_EQUIPES_TWIG', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async getTwig({ dispatch, commit }, id) {
    loading(dispatch, true)
    try {
      let result = process(await service.buscarTwigPorId(id))
      commit('SET_TWIG', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  newTwig({ dispatch }, twig) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.novoTwig(twig))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getTwigs')
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error.message, { root: true })
      }
    })
  },

  async editTwig({ dispatch }, twig) {
    loading(dispatch, true)

    try {
      let result = process(await service.atualizarTwig(twig))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getTwigs')
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async editTwigSolo({ dispatch }, twig) {
    loading(dispatch, true)

    try {
      let result = process(await service.atualizarTwig(twig))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getTwig', twig.id)
      dispatch('getTwigs')
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  delTwig({ dispatch }, id) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.deletarTwig(id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getTwigs')
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  delTwigTeam({ dispatch }, data) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.deletarTwig(data.id))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getTwigsEquipe', data.equipe)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  addMemberTwig({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.adicionarMembroTwig(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  addMultipleMembersTwig({ dispatch }, payload) {
    loading(dispatch, true)

    console.log(payload)

    let array = payload.selected
    let max = array.length

    let twig = payload.twig

    for (var i = 0; i < max; i++) {
      new Promise(async resolve => {
        try {
          let result = process(
            await service.adicionarNovoUsuarioTwig(
              twig,
              array[i].usuario.id,
              array[i].niveldeacesso
            )
          )
          console.log(result)
          resolve()
        } catch (error) {
          console.log(error)
        }
      })
    }

    loading(dispatch, false)
  },

  addUserTwig({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(
          await service.adicionarNovoUsuarioTwig(
            payload.id,
            payload.idUsuario,
            payload.nivel
          )
        )
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  changeFunder({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.alterarFinanciardor(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  changeMemberLevel({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.alterarNivelMembro(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  changeUserLevel({ dispatch }, payload) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(await service.alterarNivelUsuario(payload))
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  async getUsersTwig({ dispatch, commit }, idTwig) {
    loading(dispatch, true)

    try {
      let result = process(await service.listarUsuariosTwig(idTwig))
      commit('SET_USERS_TWIG', result.data)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  newTeamTwig({ dispatch }, data) {
    loading(dispatch, true)

    return new Promise(async resolve => {
      try {
        let result = process(
          await service.novoTwigEquipe(data.equipe, data.twig)
        )
        loading(dispatch, false)
        dispatch('message/sendSuccess', result, { root: true })
        dispatch('getTwigsEquipe', data.equipe)
        resolve()
      } catch (error) {
        loading(dispatch, false)
        dispatch('message/sendError', error, { root: true })
      }
    })
  },

  async removeUserTwig({ dispatch }, data) {
    loading(dispatch, true)

    try {
      let result = process(await service.removerUsuarioTwig(data))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getTwig', data.twig)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async removeTeamTwig({ dispatch }, data) {
    loading(dispatch, true)

    try {
      let result = process(await service.removerEquipeTwig(data))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getTwig', data.twig)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async addTeamTwig({ dispatch }, data) {
    loading(dispatch, true)

    try {
      let result = process(await service.adicionarEquipeTwig(data))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getTwig', data.twig)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_TWIGS(state, twigs) {
    state.twigs = twigs
  },

  SET_TWIG(state, twig) {
    state.twig = twig
  },

  SET_USERS_TWIG(state, usersTwig) {
    state.usersTwig = usersTwig
  },

  SET_TWIGS_EQUIPE(state, twigsEquipe) {
    state.twigsEquipe = twigsEquipe
  },

  SET_EQUIPES_TWIG(state, equipesTwig) {
    state.equipesTwig = equipesTwig
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
