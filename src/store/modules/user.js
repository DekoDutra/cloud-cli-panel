import service from '@/services/user'
import { process, loading } from '../common'

const state = {
  user: {},
  users: {}
}

const getters = {
  user: state => state.user,
  users: state => state.users
}

const actions = {
  async getMyUser({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service._buscarMeuUsuario())
      commit('SET_USER', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      window.$cookies.remove('USERTOKEN')
    }
  },

  async atualizarMyUser({ dispatch }, user) {
    loading(dispatch, true)

    try {
      let result = process(
        await service._atualizarMeuUsuario(
          window.$cookies.get('USERTOKEN'),
          user
        )
      )
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getMyUser')
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async buscarUsuarioEmailLogin({ commit }, user) {
    try {
      let result = process(
        await this.$services.user.buscarUsuarioNomeEmail(
          user,
          this.$cookies.get('USERTOKEN')
        )
      )
      console.log(result)
      commit('SET_USER', result)
    } catch (error) {
      console.log(error)
    }
  },

  async alterarSenha({ dispatch }, password) {
    loading(dispatch, true)

    try {
      let result = process(await service.alterarSenha(password))
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getMyUser')
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_USER(state, user) {
    state.user = user
  },
  SET_USERS(state, users) {
    state.users = users
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
