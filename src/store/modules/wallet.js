import service from '@/services/wallet'
import { loading, process } from '../common'

const state = {
  wallet: {},
  payments: [],
  boletos: []
}

const getters = {
  wallet: state => state.wallet,
  payments: state => state.payments,
  boletos: state => state.boletos
}

const actions = {
  async getWallet({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.buscarCarteira())
      commit('SET_WALLET', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },

  async pagamentoRede({ dispatch }, payload) {
    loading(dispatch, true)
    try {
      let result = process(await service.pagamentoRede(payload))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getPayments')
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },
  async pagamentoBoletoPagarme({ dispatch }, saldo) {
    loading(dispatch, true)
    try {
      let result = process(await service.pagamentoBoletoPagarme(saldo))
      loading(dispatch, false)
      dispatch('message/sendSuccess', result, { root: true })
      dispatch('getBoletos')
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },
  async getPayments({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.buscarHistorico())
      commit('SET_PAYMENTS', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  },
  async getBoletos({ dispatch, commit }) {
    loading(dispatch, true)

    try {
      let result = process(await service.buscarBoletos())
      commit('SET_BOLETOS', result)
      loading(dispatch, false)
    } catch (error) {
      loading(dispatch, false)
      dispatch('message/sendError', error, { root: true })
    }
  }
}

const mutations = {
  SET_WALLET(state, wallet) {
    state.wallet = wallet
  },
  SET_PAYMENTS(state, payments) {
    state.payments = payments
  },
  SET_BOLETOS(state, boletos) {
    state.boletos = boletos
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
