import Vue from 'vue'
import Vuex from 'vuex'

import modules from './modules/index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user: modules.user,
    message: modules.message,
    wallet: modules.wallet,
    adress: modules.adress,
    phone: modules.phone,
    credit: modules.credit,
    lembrete: modules.lembrete,
    teams: modules.teams,
    loading: modules.loading,
    twig: modules.twig,
    convite: modules.convite,
    leaves: modules.leaves,
    leafPlan: modules.leafPlan,
    servers: modules.servers,
    logs: modules.logs,
    tabs: modules.tabs,
    postgres: modules.postgres,
    postgresPlan: modules.postgresPlan,
    mysql: modules.mysql,
    mysqlPlan: modules.mysqlPlan,
    lake: modules.lake,
    payment: modules.payment,
    tickets: modules.tickets
  }
})
